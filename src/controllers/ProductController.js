const Product = require('../models/Product');


async function create (req, res) {
    try {
        const product = await Product.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso", Product: product});
    } catch(err) {
        return res.status(500).json(err);
    }
}

async function index (req,res){
    try {
        const products = await Product.findAll();
        return res.status(200).json({message: "todos os usuários listados", Products: products});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function show (req,res){
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id, {include: [{
            model: Address
        }]
    });
        return res.status(200).json({message: "Usuário encontrado", Product: product});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function update (req,res){
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated){
            const product = await Products.findByPk(id);
            return res.status(200).json(product);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("usuário não encontrado");
    }
}

async function destroy (req,res){
    const {id} = req.params;
    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Usuário deletado");
        }
    
        throw new Error();
    
    } catch (e) {
        return res.status(500).json("Usuário não encontrado");
    }
}

async function addUser (req, res) {
  const {userId, productId} = req.params;
  try {
      const user = await User.findByPk(userId);
      const product = await Product.findByPk(productId);
      await product.setUser(user);
      return res.status(200).json(product);
  } catch (error) {
      return res.status(500).json({error});
  }
}

async function removeUser (req, res) {
  const {userId, productId} = req.params;
  try {
      const user = await User.findByPk(userId);
      const product = await Product.findByPk(productId);
      await product.setUser(null);
      return res.status(200).json(product);
  } catch (error) {
      return res.status(500).json({error});
  }
}




module.exports = {
    create,
    index, 
    show,
    update,
    destroy,
    addUser,
    removeUser,
}

