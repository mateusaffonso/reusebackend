const Order = require('../models/Order');


async function create (req, res) {
    try {
        const order = await Order.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso", Order: order});
    } catch(err) {
        return res.status(500).json(err);
    }
}

async function index (req,res){
    try {
        const orders = await Order.findAll();
        return res.status(200).json({message: "todos os usuários listados", Orders: orders});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function show (req,res){
    const {id} = req.params;
    try {
        const order = await Order.findByPk(id, {include: [{
            model: Address
        }]
    });
        return res.status(200).json({message: "Usuário encontrado", Order: order});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function update (req,res){
    const {id} = req.params;
    try {
        const [updated] = await Order.update(req.body, {where: {id: id}});
        if(updated){
            const order = await Order.findByPk(id);
            return res.status(200).json(order);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("usuário não encontrado");
    }
}

async function destroy (req,res){
    const {id} = req.params;
    try {
        const deleted = await Order.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Usuário deletado");
        }
    
        throw new Error();
    
    } catch (e) {
        return res.status(500).json("Usuário não encontrado");
    }
}

async function addUser (req, res) {
  const {userId, orderId} = req.params;
  try {
      const user = await User.findByPk(userId);
      const order = await Order.findByPk(orderId);
      await order.setUser(user);
      return res.status(200).json(order);
  } catch (error) {
      return res.status(500).json({error});
  }
}

async function removeUser (req, res) {
  const {userId, orderId} = req.params;
  try {
      const user = await User.findByPk(userId);
      const order = await Order.findByPk(orderId);
      await order.setUser(null);
      return res.status(200).json(order);
  } catch (error) {
      return res.status(500).json({error});
  }
}

async function addProduct (req, res) {
  const {orderId, productId} = req.params;
  try {
      const order = await Order.findByPk(orderId);
      const product = await Product.findByPk(productId);
      await product.setUser(order);
      return res.status(200).json(product);
  } catch (error) {
      return res.status(500).json({error});
  }
}

async function removeProduct (req, res) {
  const {orderId, productId} = req.params;
  try {
      const order = await Order.findByPk(orderId);
      const product = await Product.findByPk(productId);
      await product.setUser(null);
      return res.status(200).json(product);
  } catch (error) {
      return res.status(500).json({error});
  }
}




module.exports = {
    create,
    index, 
    show,
    update,
    destroy,
    addUser,
    removeUser,
    addProduct,
    removeProduct,
}

