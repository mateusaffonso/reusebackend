const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    photo:{
      type: DataTypes.TEXT,
      allowNull:false
    }

});

User.associate = function(models) {
    User.hasMany(models.Order);
    User.hasMany(models.Product);
};

module.exports = User;
