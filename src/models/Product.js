const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    productStatus:{
        type: DataTypes.STRING,
        allowNull:false
    },
    inStock: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    // userPostedId: {
    //     type: DataTypes.DOUBLE,
    //     allowNull: false
    // },
    photo:{
        type: DataTypes.TEXT,
        allowNull:false
    }
});

Product.associate = function(models) {
    Product.belongsTo(models.User);
    Product.hasMany(models.Order);
};


module.exports = Product;
