const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Order = sequelize.define('Order', {

    // products: {
    //   type: DataTypes.TEXT,
    //   allowNull: false  
    // },
    totalPrice: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    paymentMethod: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false
    },
});

Order.associate = function(models) {
    Order.belongsTo(models.User);
    Order.hasMany(models.Product);
};

module.exports = Order;