const Express = require('express');
const router = Express();
const UserController = require('../controllers/UserController');
const OrderController = require('../controllers/OrderController');
const ProductController = require('../controllers/ProductController');

//rotas para o user
router.post('/user', UserController.create);
router.get('/user/:id', UserController.show);
router.get('/user', UserController.index);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.destroy);

//rotas para o Order
router.post('/order', OrderController.create);
router.get('/order/:id', OrderController.show);
router.get('/order', OrderController.index);
router.put('/order/:id', OrderController.update);
router.delete('/order/:id', OrderController.destroy);

//rotas para o Product
router.post('/product', ProductController.create);
router.get('/product/:id', ProductController.show);
router.get('/product', ProductController.index);
router.put('/product/:id', ProductController.update);
router.delete('/product/:id', ProductController.destroy);


//rota pra relação user-order
router.put('/user/:userId/order/:orderId', OrderController.addUser);
router.delete('/user/:userID/order/:orderId', OrderController.removeUser)

//rota pra relação user-product
router.put('/user/:userId/produtc/:productId', ProductController.addUser);
router.delete('/user/:userID/product/:productId', ProductController.removeUser)

//rota pra relação order-product
router.put('/product/:productId/order/:orderId', OrderController.addUser);
router.delete('/product/:productId/order/:orderId', OrderController.removeUser)


module.exports = router;

